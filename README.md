## Important notice  
  
The *C code* in this repository is written using *Visual Studio Code* as an editor and *MinGW* as compiler.  
There may occur problems when using other compilers.  
Other tasks are written using *C#* as language. 
  
Please be aware, that the code here is just for help and learning purposes.  
**Using ths code exactly as is in your own hand in or homework is strictly forbidden**  
  
Feel free to contribute your code after taking a look at the *CONTRIBUTING.md* file.